'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _MenuItem = require('material-ui/MenuItem');

var _MenuItem2 = _interopRequireDefault(_MenuItem);

var _SelectField = require('material-ui/SelectField');

var _SelectField2 = _interopRequireDefault(_SelectField);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _CircularProgress = require('material-ui/CircularProgress');

var _CircularProgress2 = _interopRequireDefault(_CircularProgress);

var _AutoComplete = require('material-ui/AutoComplete');

var _AutoComplete2 = _interopRequireDefault(_AutoComplete);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _propTypes = require('prop-types');

var _propTypes2 = _interopRequireDefault(_propTypes);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var createReactClass = require('create-react-class');

// This config below is for easy access to add more object below
// Most of this script runs generically thus the config below will fit in 99.9% of the times
// TODO: Make a global nonAttribute list instead per instance

var screenConfig = {
  // This is the state applied to the type of object on startup.
  // The state object usually contains a random uui for example 0000-0000-0000-0000-selectable
  // Thus the object will always contain a state for that text that will not conflict by is created in a dynamic way
  initialState: {
    button: ["selectable", "selected", "error"],
    textfield: ["value", "activeError"],
    text: [],
    dropdown: ["value", "selected", {
      id: "dropdown-source", type: "array"
    }],
    autocomplete: [{
      id: "data-source", type: "array"
    }]
  },
  // Restriction contains all the pure information for the element
  // This means: What can the object process (attribute based or not), what happens when a certain action is activated/clicked and/or how the element is generate. (The component/element restriction)
  restrictions: {
    text: {
      // These are attributes that will be excluded when doing a 'all json fields == attributes: id=value'.
      // If for example you leave out body the generic code will render a element like this: <li body={}></li>
      // Almost all components that do not take a body attribute will then complain
      nonAttributes: ["text", "style", "action", "body", "save", "setState"],
      // This is a reference to the component that needs to be generate. i == attributes, ii == body
      element: function element(i, ii) {
        return _react2.default.createElement(
          'li',
          i,
          ii
        );
      }
    },

    button: {
      nonAttributes: ["selected", "selectable", "type", "group", "invertGroup", "groupOrder", "style", "action", "body", "dispatch", "dataSource", "group", "setState"],
      element: function element(i) {
        return _react2.default.createElement(_FlatButton2.default, i);
      },

      // Actions contains all the actions that needs to happen on the id. id == action, value == action function
      actions: {
        onTouchTap: "buttonTouchTap",
        onClick: "buttonAction"
      }
    },

    textfield: {
      nonAttributes: ["label", "group", "groupOrder", "errorText", "style", "action", "body", "dispatch", "dataSource", "group", "setState"],
      element: function element(i) {
        return _react2.default.createElement(_TextField2.default, i);
      },

      actions: {
        onChange: "textFieldChange",
        onBlur: "textFieldBlur"
      }
    },

    dropdown: {
      nonAttributes: ["style", "action", "body", "dataSource", "dispatch", "options", "group", "setState"],
      element: function element(i, ii) {
        return _react2.default.createElement(
          _SelectField2.default,
          i,
          ii
        );
      },

      actions: {
        onChange: "dropdownChange"
      }
    },

    autocomplete: {
      nonAttributes: ["style", "action", "body", "dataSource", "dispatch", "group", "setState"],
      element: function element(i) {
        return _react2.default.createElement(_AutoComplete2.default, i);
      },

      actions: {}
    }
  }

  // Generate a uuid - This will be used all over
};var guid = function guid() {
  var s4 = function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  };
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
};

// This prevents a infinite loop. Reason: Dynamic Content, Dynamic State, Dynamic State Set, Generic Functions can equal a setState loop
// This needs to be outside of the state and props sections and be unique per import
var doneState = false;

var screenProcessor = createReactClass({
  displayName: 'screenProcessor',
  getInitialState: function getInitialState() {
    return {
      // The component should only pre process state on props change once per component mounting
      initialProcessingStepComplete: false,
      // All the available actions the the current screen
      actions: {},
      // Is the rendered screen a dialog popup
      dialog: false,
      // The id of the currently rendered screen
      active: '',
      // The id of the currently rendered popup screen (A popup screen is a screen over any other active screen)
      activePopup: ''
    };
  },


  propTypes: {
    emitter: _propTypes2.default.object.isRequired,
    dispatcher: _propTypes2.default.object.isRequired,

    // The json data that needs to be processed into a material design html format
    data: _propTypes2.default.object.isRequired,

    // The object of functions used for callback processing
    processors: _propTypes2.default.object.isRequired
  },

  // Primary render function - This function renders both the active screen and the active popup screen
  render: function render() {
    console.log("-- Latest Screen Props and State --");
    console.log(this.props);
    console.log(this.state);
    return _react2.default.createElement(
      'span',
      null,
      this.generateActiveScreen(this.state.active),
      typeof this.state.activePopup !== null ? this.generateActiveScreen(this.state.activePopup, true) : ""
    );
  },


  // This function catches any updates to the component. As soon as the component gets valid data a pre processor is kicked of which then locks the initial render (a.k.a this function from resetting the state again)
  componentWillUpdate: function componentWillUpdate(data, state) {
    this.processors = this.props.processors;
    if (data !== null && !this.state.initialProcessingStepComplete) {
      console.warn("~> Processing a new state");
      this.getScreenReturned(data.data, state);
      state.initialProcessingStepComplete = true;
    }
  },


  /** This is step one to the entire processing of the dynamic components -
   * 1) Set a new dynamic state without a re-render
   * ** This is a core function */
  // Generate the state variables required for the active returned api screen
  getScreenReturned: function getScreenReturned(data, preState) {
    var _this = this;

    var failure = false;
    if (typeof data.init !== 'string') {
      failure = true;
      console.error("No top level `init` (String) field found in your json object - The init field is used to initialise a action screen by this id");
    }

    if (typeof data.dialog !== 'boolean') {
      failure = true;
      console.error("No top level `dialog` (Boolean) field found in your json object - The init field is used to mark the entire process as a dialog popup or not");
    }

    if (_typeof(data.actions) !== 'object') {
      failure = true;
      console.error("No top level `actions` (Object) field found in your json object");
    } else {
      Object.keys(data.actions).map(function (action) {
        if (typeof data.actions[action].title === 'undefined') {
          console.error('Actions item \'' + action + '\' requires a \'title\' (String, Dispatch or Pure State) json field');
          failure = true;
        }
      });
    }

    if (typeof data.init === 'string' && _typeof(data.actions) === 'object') {
      if (typeof data.actions[data.init] === 'undefined') {
        console.error('Init can\'t find a action in \'actions\' with the name \'' + data.init + '\'');
        failure = true;
      }
    }

    if (failure) return {};

    // The code below generates and populates the state object
    // This object is used to set dynamic state from the screenConfig object
    var state = {};
    Object.keys(typeof data.actions === 'undefined' ? [] : data.actions).map(function (key) {
      (typeof data.actions[key].body === 'undefined' ? [] : data.actions[key].body).map(function (item) {
        // Unique id for the session
        var id = guid();
        // Temp object used to store temp state that will later on be applied to the actual state
        var temp = {};
        var listOfStates = screenConfig.initialState[Object.keys(item).filter(function (key) {
          return key !== "id";
        })[0].toLowerCase()];
        listOfStates = typeof listOfStates === 'undefined' ? [] : listOfStates;
        listOfStates.map(function (initState) {
          // The object in the config can either be defined as a json object or a pure string
          // Default return type for a object is always a string. To set a type for a object use the json object
          if ((typeof initState === 'undefined' ? 'undefined' : _typeof(initState)) === 'object') {
            if (initState.type.toLowerCase() === 'array') return temp[id + '-' + initState.id] = [];else if (initState.type.toLowerCase() === 'string') return temp[id + '-' + initState.id] = '';else if (initState.type.toLowerCase() === 'boolean') return temp[id + '-' + initState.id] = false;else if (initState.type.toLowerCase() === 'number') return temp[id + '-' + initState.id] = 0;else {
              // Replace activeError with the error name. Makes dynamic content easier on error text
              return temp[id + '-' + (initState === 'activeError' ? 'error' : initState)] = typeof item[Object.keys(item).filter(function (key) {
                return key !== "id";
              })[0].toLowerCase()][initState] === 'undefined' ? '' : item[Object.keys(item).filter(function (key) {
                return key !== "id";
              })[0].toLowerCase()][initState];
            }
          } else {
            // Replace activeError with the error name. Makes dynamic content easier on error text
            return temp[id + '-' + (initState === 'activeError' ? 'error' : initState)] = typeof item[Object.keys(item).filter(function (key) {
              return key !== "id";
            })[0].toLowerCase()][initState] === 'undefined' ? '' : item[Object.keys(item).filter(function (key) {
              return key !== "id";
            })[0].toLowerCase()][initState];
          }
        });
        // Assign a randomly generate uuid to every single object. Makes future state control easier
        Object.assign(item, { "id": id });
        // Assign the state variables created and set within temp to the state temp object which will later on be applied to the actual state
        Object.assign(state, temp);
      });

      // Apply a id and state to the title if required
      var title = typeof data.actions[key].title === 'undefined' ? [] : data.actions[key].title;
      if ((typeof title === 'undefined' ? 'undefined' : _typeof(title)) === 'object') {
        var id = guid();
        var temp = {};
        temp[id + '-' + title.state] = typeof title.default !== 'undefined' ? title.default : "--";
        Object.assign(state, temp);
        Object.assign(data.actions[key].title, { "id": id });
      }
    });

    var removeListeners = [];
    var addListeners = [];
    var fullListeners = [];
    // This mapping will populate the listener arrays.
    // removeListeners will contain all the objects that needs to be 'emitter.removeAllListeners'
    // addListeners will contain all the objects that needs to be 'emitter.on'
    Object.keys(data.actions).map(function (key) {
      if (typeof data.actions[key].title["id"] !== 'undefined') {
        // Listener on the title object
        addListeners.push([data.actions[key].title["id"], data.actions[key].title]);
      }

      data.actions[key].body.map(function (body) {
        var mainKey = Object.keys(body).filter(function (key) {
          return key !== "id";
        })[0];
        if (typeof mainKey !== 'undefined') {
          Object.keys(body[mainKey]).map(function (item) {
            // If the object in your specified json object contains a .emitter and a .processor then it is awaiting data
            if (typeof body[mainKey][item].emitter !== 'undefined' && typeof body[mainKey][item].processor !== 'undefined') {
              removeListeners.push(body[mainKey][item].emitter);
              addListeners.push([body["id"], body[mainKey][item]]);
            }
            if (typeof body[mainKey][item].state !== 'undefined') {
              fullListeners.push([body["id"], body[mainKey][item]]);
            }
          });
        }
      });
    });

    // Creates a unique list and then removes the listeners. [a,a,b,c,c,c] == [a,b,c]
    removeListeners.filter(function (value, index, self) {
      return self.indexOf(value) === index;
    }).map(function (item) {
      return _this.props.emitter.removeAllListeners(item);
    });

    // Group all the listeners that has the same emitter
    // Reason why we group them is that all the objects that requires the same emitter can be kicked of at the same time with a single emitter. 1 emitter, multiple functions
    var groupedItems = {};
    addListeners.map(function (item) {
      if (typeof groupedItems[item[1].emitter] === 'undefined') groupedItems[item[1].emitter] = [[item[0], item[1].processor, item[1].emitter, item[1].state]];else groupedItems[item[1].emitter].push([item[0], item[1].processor, item[1].emitter, item[1].state]);
    });

    // Apply a single emitter to a emitter id but with multiple functions for all the grouped listeners
    Object.keys(groupedItems).map(function (key) {
      // Display a emitter connection
      groupedItems[key].map(function (to) {
        return console.warn('~> Applying a emitter to "' + to[0] + '"');
      });

      // Connect to emitter
      _this.props.emitter.on(key, function (emitterA, emitterB, emitterC) {
        groupedItems[key].map(function (to) {
          (function () {
            var thisScope = _this;
            var addons = { setState: function setState(data) {
                _this.setState(_defineProperty({}, to[0] + '-' + to[3], data));
              } };
            var merge = _extends({}, thisScope, addons);
            _this.processors[to[1]].apply(merge, [to[0], emitterA, emitterB, emitterC, to[3]]);
          })();
        });
      });
    });

    // Generate all the state defined objects for the emitter and processor
    fullListeners.map(function (item) {
      if (typeof item[1].state !== 'undefined') {
        _this.assignDynamicState(state, item[0] + '-' + item[1].state, typeof item[1].default === 'undefined' ? "" : item[1].default);
      }
    });

    this.runScreenInitFunctions(data.actions[data.init], state, function () {
      // Assign all the objects to a preState object
      Object.assign(preState, _extends({
        active: data.init,
        actions: data.actions,
        dialog: data.dialog
      }, state));
    });
  },


  /** This is step two to the entire processing of the dynamic components -
   * 2) On any action or parent action when a screen is changed a new actionId is passed to this function which then generates a new screen based on the id
   * ** This is a core function */
  // Generate a new screen with all the relevant data - This happens when the state changes
  generateActiveScreen: function generateActiveScreen(actionId) {
    var _this2 = this;

    var forceDialog = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;

    // If the actionId is not valid then a active screen can not be generated. actionId == the screen within the json object
    if (typeof actionId === 'undefined' || actionId == null || actionId == '' || actionId == 'undefined') return null;else {
      // First we check if the screen id you are requesting does exist if not a empty will be generated and a console error will be produced
      var screen = this.state.actions[actionId];
      if (typeof screen === 'undefined') {
        console.error('~> A screen with the id \'' + actionId + '\' not found');
        return null;
      }

      // Generate all the primary buttons that is going to be used within this screen
      // This is all the buttons in either the top of bottom of the window
      // They get generated separately because generally this affects the actual window and not data
      var buttons = (typeof screen.buttons === 'undefined' ? [] : screen.buttons).map(function (button) {
        var generatedButton = _react2.default.createElement(_RaisedButton2.default, { secondary: typeof button.secondary === 'undefined' ? false : button.secondary, key: guid(), label: button.label, onClick: function onClick() {
            _this2.buttonAction(null, button.action);
          } });
        if (_this2.state.dialog || forceDialog) return generatedButton;else return _react2.default.createElement(
          'div',
          { key: button.label, className: 'search_result_card_button' },
          generatedButton
        );
      });

      // This function generates, assigns, structures and applies all the correct data to the elements.
      /** If any special processing is required it will happen here
       * for example a button gets a special color when a type is defined
       * */
      var spawnElement = function spawnElement(id, item, original) {
        // Currently all objects that can be used needs to be defined in the restrictions object
        // TODO: Remove this and make it more generic - Easier for future use
        if (typeof screenConfig.restrictions[id] === 'undefined') {
          console.error('~> \'' + id + '\' not found in the restrictions object');
          return _react2.default.createElement('div', null);
        }

        // Id, name and key is preset to the actions object
        // All extra actions on a element is contained in the actions json
        var actions = { id: item.id, name: item.id, key: item.id

          // Link all element type action to functions
        };if (typeof screenConfig.restrictions[id].actions !== 'undefined') {
          Object.keys(screenConfig.restrictions[id].actions).map(function (action) {
            _this2.assignDynamicState(actions, action, function () {
              for (var _len = arguments.length, values = Array(_len), _key = 0; _key < _len; _key++) {
                values[_key] = arguments[_key];
              }

              return _this2[screenConfig.restrictions[id].actions[action]].apply(_this2, [actions, item.id].concat(values));
            });
          });
        }

        // Apply all the extra objects as attributes
        var attributes = {};
        Object.assign(attributes, item[id]);
        Object.keys(item[id]).map(function (i) {
          return screenConfig.restrictions[id].nonAttributes.indexOf(i) !== -1 ? delete actions[i] : null;
        });
        Object.keys(item[id]).map(function (i) {
          return screenConfig.restrictions[id].nonAttributes.indexOf(i) !== -1 ? delete attributes[i] : null;
        });

        // Add a default style object
        var tempStyle = typeof item[id].style !== 'undefined' ? item[id].style : {};
        Object.assign(actions, { style: _extends({}, tempStyle, { marginBottom: "14px" }) });
        // Add a default margin - Improves the look and feel

        if (id === 'button') {
          // Assign a state to a label if required.
          // Needs to be hardcoded because of the 'not_int' for word testing
          var label = item[id].label;
          if (_typeof(item[id].label) === 'object') {
            if (typeof item[id].label.state !== 'undefined') {
              label = _this2.state[item.id + '-' + item[id].label.state];
            }
          }

          // Applies a color and/or the not_int flag
          Object.assign(actions, { className: 'button_' + (typeof item[id].type === 'undefined' ? "" : item[id].type) + ' ' + (_this2.state[item.id + '-selected'] === false || _this2.state[item.id + '-selected'] === '' ? "" : "selected") + ' ' + (/\s+/ig.test(label) ? "not_int" : "") });

          // Make sure the component does not break
          if (typeof item[id].label === 'undefined') {
            _this2.assignDynamicState(actions, "label", " ");
          }
        }

        // If the object is a textfield some special attention is required
        if (id === 'textfield') {
          Object.assign(attributes, { value: _this2.state[item.id + '-value'], errorText: _this2.state[item.id + '-error'] });
        }

        // If the object is a dropdown some special attention is required
        if (id === 'dropdown') {
          if (_typeof(item[id].options) === 'object' && typeof item[id].options.emitter !== 'undefined') {
            item[id].body = _this2.state[item.id + '-dropdown-source'].map(function (x) {
              return _react2.default.createElement(_MenuItem2.default, { key: guid(), value: x, label: x, primaryText: x });
            });
            Object.assign(actions, {
              onChange: function onChange(event, index, value) {
                return _this2.dropDownChange(item.id, event, index, value);
              },
              value: _this2.state[item.id + '-selected']
            });
          } else {
            item[id].body = item[id].options.map(function (i) {
              return _react2.default.createElement(_MenuItem2.default, { key: guid(), value: i.value, label: i.body, primaryText: i.body });
            });
            Object.assign(actions, {
              onChange: function onChange(event, index, value) {
                return _this2.dropDownChange(item.id, event, index, value);
              },
              value: _this2.state[item.id + '-selected']
            });
          }
        }

        // On all dataSources defined we need to set a object state
        // TODO: Make more generic
        if (typeof item[id].dataSource !== 'undefined') {
          Object.assign(attributes, {
            dataSource: _this2.state[item.id + '-data-source']
          });
        }

        // Make sure we do not have any undefined objects that needs to be destructured
        original = typeof original === 'undefined' ? {} : original;
        actions = typeof actions === 'undefined' ? {} : actions;
        attributes = typeof attributes === 'undefined' ? {} : attributes;

        // Assign the user stated state values
        // assignedBody contains the original body and will be modified if a state is set
        var assignedBody = item[id].body;
        Object.keys(item[id]).map(function (key) {
          if (typeof item[id][key].state !== 'undefined') {
            if (key === "body") {
              assignedBody = _this2.state[item.id + '-' + item[id][key].state];
            } else {
              _this2.assignDynamicState(attributes, '' + key, _this2.state[item.id + '-' + item[id][key].state]);
            }
          }
        });

        // Remove all attributes from the actions object that contains private fields. For example emitter and processor objects
        Object.keys(actions).map(function (key) {
          if (typeof actions[key] !== "undefined") {
            if (typeof actions[key].emitter !== 'undefined' || typeof actions[key].processor !== 'undefined' || typeof actions[key].state !== 'undefined') {
              delete actions[key];
            }
          }
        });

        // Remove all attributes from the attributes object that contains private fields. For example emitter and processor objects
        Object.keys(attributes).map(function (key) {
          if (typeof attributes[key] !== "undefined") {
            if (typeof attributes[key].emitter !== 'undefined' || typeof attributes[key].processor !== 'undefined' || typeof attributes[key].state !== 'undefined') {
              delete attributes[key];
            }
          }
        });

        // Generate the element
        return screenConfig.restrictions[id].element(_extends({}, actions, original, attributes), typeof assignedBody !== 'undefined' ? assignedBody : '');
      };

      /**
       A done state is checked so that we don't run into a infinite loop.
       Render =>
       ^ Render dispatch events from json =>
       |   On render set a dynamic state for the new components =>
       |     If re-render the Render function is called again >-|
       |                                                        |
       -----<---------<---------------<------------<-----------
       Thus we are setting a done state on the initial render
       */

      // This prevents a infinite loop. Reason: Dynamic Content, Dynamic State, Dynamic State Set, Generic Functions can equal a setState loop
      // A dispatch event will run return setState and then the component will try to reRender and redispatch
      if (!doneState) {
        doneState = true;
        if (typeof this.state.actions[actionId].dispatch !== 'undefined') {
          this.state.actions[actionId].dispatch.map(function (dis) {
            if (typeof dis.type !== 'undefined') {
              _this2.props.dispatcher.dispatch({ type: dis.type });
            }
          });
        }
      }
      var dynamicTitle = typeof screen.title === 'string' ? screen.title : this.state[screen.title.id + '-' + screen.title.state];

      var screenObjects = _react2.default.createElement(
        'span',
        null,
        _react2.default.createElement(
          'div',
          { className: 'title', style: { marginBottom: "15px", padding: "5px 3px 10px 3px", borderBottom: "1px solid #EBEBEB", fontSize: "20px", width: "100%" } },
          dynamicTitle
        ),
        _react2.default.createElement(
          'div',
          { className: 'button_bar_dialer', style: { width: "100%", float: "left" } },
          _react2.default.createElement(
            'div',
            { className: 'buttons', style: { position: "relative", top: "0px" } },
            _react2.default.createElement(
              'div',
              { className: 'middled', style: { width: "100%", float: "left", clear: "both", paddingBottom: "15px", borderBottom: "1px solid #EBEBEB", marginBottom: "15px" } },
              buttons
            )
          )
        ),
        _react2.default.createElement(
          'ul',
          { style: { float: "left", clear: "both" }, className: 'script' },
          (typeof screen.body === 'undefined' ? [] : screen.body).map(function (item) {
            return spawnElement(Object.keys(item).filter(function (key) {
              return key !== "id";
            })[0].toLowerCase(), item);
          })
        )
      );

      if (typeof screen.config !== 'undefined') {
        if (typeof screen.config.buttons !== 'undefined') {
          if (typeof screen.config.buttons.position !== 'undefined') {
            if (screen.config.buttons.position === 'bottom') {
              screenObjects = _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement(
                  'div',
                  { className: 'title', style: { marginBottom: "15px", padding: "5px 3px 10px 3px", borderBottom: "1px solid #EBEBEB", fontSize: "20px", width: "100%" } },
                  dynamicTitle
                ),
                _react2.default.createElement(
                  'ul',
                  { style: { float: "left", clear: "both" }, className: 'script' },
                  (typeof screen.body === 'undefined' ? [] : screen.body).map(function (item) {
                    return spawnElement(Object.keys(item).filter(function (key) {
                      return key !== "id";
                    })[0].toLowerCase(), item);
                  })
                ),
                _react2.default.createElement(
                  'div',
                  { className: 'button_bar_dialer', style: { width: "100%", float: "left" } },
                  _react2.default.createElement(
                    'div',
                    { className: 'buttons', style: { position: "relative", top: "0px" } },
                    _react2.default.createElement(
                      'div',
                      { className: 'middled', style: { width: "100%", float: "left", clear: "both", paddingTop: "15px", borderTop: "1px solid #EBEBEB", marginTop: "15px", paddingBottom: "8px" } },
                      buttons
                    )
                  )
                )
              );
            } else if (screen.config.buttons.position === 'left') {
              screenObjects = _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement(
                  'div',
                  { style: { borderBottom: "1px solid #EBEBEB", padding: "5px 3px 10px 3px", marginBottom: "15px", float: "left", width: "100%" } },
                  _react2.default.createElement(
                    'div',
                    { className: 'button_bar_dialer', style: { width: "50%", float: "left" } },
                    _react2.default.createElement(
                      'div',
                      { className: 'buttons', style: { position: "relative", top: "0px" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'middled', style: { width: "100%", float: "left", clear: "both", paddingBottom: "5px" } },
                        buttons
                      )
                    )
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'title', style: { fontSize: "20px", width: "50%", float: "left" } },
                    dynamicTitle
                  )
                ),
                _react2.default.createElement(
                  'ul',
                  { style: { float: "left", clear: "both" }, className: 'script' },
                  (typeof screen.body === 'undefined' ? [] : screen.body).map(function (item) {
                    return spawnElement(Object.keys(item).filter(function (key) {
                      return key !== "id";
                    })[0].toLowerCase(), item);
                  })
                )
              );
            } else if (screen.config.buttons.position === 'right') {
              screenObjects = _react2.default.createElement(
                'span',
                null,
                _react2.default.createElement(
                  'div',
                  { style: { borderBottom: "1px solid #EBEBEB", padding: "5px 3px 10px 3px", marginBottom: "15px", float: "left", width: "100%" } },
                  _react2.default.createElement(
                    'div',
                    { className: 'title', style: { fontSize: "20px", width: "50%", float: "left" } },
                    dynamicTitle
                  ),
                  _react2.default.createElement(
                    'div',
                    { className: 'button_bar_dialer', style: { paddingLeft: "5%", width: "45%", float: "left" } },
                    _react2.default.createElement(
                      'div',
                      { className: 'buttons', style: { position: "relative", top: "0px" } },
                      _react2.default.createElement(
                        'div',
                        { className: 'middled', style: { width: "100%", float: "left", clear: "both", paddingBottom: "5px" } },
                        buttons
                      )
                    )
                  )
                ),
                _react2.default.createElement(
                  'ul',
                  { style: { float: "left", clear: "both" }, className: 'script' },
                  (typeof screen.body === 'undefined' ? [] : screen.body).map(function (item) {
                    return spawnElement(Object.keys(item).filter(function (key) {
                      return key !== "id";
                    })[0].toLowerCase(), item);
                  })
                )
              );
            }
          }
        }
      }

      // Either a dialog or a normal div is generated. All depends on the dialog flag
      if (this.state.dialog || forceDialog) {
        return _react2.default.createElement(
          _Dialog2.default,
          { bodyClassName: 'decline_reason_class', contentClassName: 'decline_reason_class', title: dynamicTitle, actions: buttons, modal: false, open: true },
          (typeof screen.body === 'undefined' ? [] : screen.body).map(function (item) {
            return spawnElement(Object.keys(item).filter(function (key) {
              return key !== "id";
            })[0].toLowerCase(), item);
          })
        );
      } else {
        return _react2.default.createElement(
          'div',
          { className: 'transcript_details' },
          _react2.default.createElement(
            _Paper2.default,
            { className: 'transcript_paper', style: { float: "left", width: "100%", clear: "both" } },
            screenObjects
          )
        );
      }
    }
  },


  // When a button is pressed set the new state to a active id
  setActiveScreen: function setActiveScreen(actionId) {
    doneState = false;
    console.log('~> Set active screen to "' + actionId + '"');
    this.setState({ active: actionId });
  },


  // When a button is pressed set the new state to a active id
  setActivePopup: function setActivePopup(actionId) {
    doneState = false;
    console.log('~> Set active popup screen to "' + actionId + '"');
    this.setState({ activePopup: actionId });
  },
  dropDownChange: function dropDownChange(actions, actionId, event, index, value) {
    this.setDynamicState(actionId + '-selected', value);
  },


  // TODO: Pure state done
  // On text field change action
  textFieldChange: function textFieldChange(actions, itemId, obj, value) {
    var oldPureState = this.state.pureState;
    var extraState = { pureState: _extends({}, oldPureState) };

    var i = this.state.actions[this.state.active].body.filter(function (item) {
      return item.id === itemId;
    })[0];
    var ii = i[Object.keys(i).filter(function (key) {
      return key !== "id";
    })[0]];
    if (typeof ii.setState !== 'undefined') {
      extraState.pureState[ii.setState] = value;
    }

    if (typeof actions.setState !== 'undefined') {
      var _extends2;

      this.setState(_extends((_extends2 = {}, _defineProperty(_extends2, itemId + '-value', value), _defineProperty(_extends2, actions.setState, value), _extends2), extraState));
    } else {
      this.setState(_extends(_defineProperty({}, itemId + '-value', value), extraState));
    }
  },


  // On text field blur
  textFieldBlur: function textFieldBlur(actions, itemId, obj, value) {
    this.setDynamicState(itemId + '-error', typeof this.state[itemId + '-value'] === 'undefined' || this.state[itemId + '-value'] == '' || this.state[itemId + '-value'] == null ? this.state.actions[this.state.active].body.filter(function (i) {
      return i.id === itemId;
    })[0].textfield.errorText : '');
  },
  runScreenInitFunctions: function runScreenInitFunctions(data, state, continueFunc) {
    var _this3 = this;

    if (typeof data.func !== 'undefined') {
      data.func.map(function (func) {
        var _props$processors$fun;

        var parameters = (typeof func.callStateParameters === 'undefined' ? [] : func.callStateParameters).map(function (text) {
          return typeof _this3.state.pureState === 'undefined' ? "" : typeof _this3.state.pureState[text] === 'undefined' ? "" : _this3.state.pureState[text];
        });

        var runApply = function runApply(funcResult, funcResultState) {
          func.applyTo.map(function (to) {
            // Apply the value to the title object - seperate cause its a high class object - outside of the body
            if (data.title.state !== 'undefined') {
              if (data.title.state === to.state) {
                Object.assign(state, _defineProperty({}, data.title.id + '-' + data.title.state, funcResult));
              }
            }

            data.body.map(function (bodyItem) {
              var primary = Object.keys(bodyItem).filter(function (key) {
                return key !== "id";
              })[0];
              Object.keys(bodyItem[primary]).map(function (internalItem) {
                if (typeof bodyItem[primary][internalItem].state !== 'undefined') {
                  if (bodyItem[primary][internalItem].state === to.state) {
                    var temp = _extends({}, funcResultState);
                    if (funcResult !== null) {
                      temp[bodyItem.id + '-' + bodyItem[primary][internalItem].state] = funcResult;
                    }
                    Object.assign(state, temp);
                  }
                }
              });
            });
          });
        };

        var funcResult = (_props$processors$fun = _this3.props.processors[func.call]).call.apply(_props$processors$fun, [_this3].concat(_toConsumableArray(parameters)));

        if (typeof funcResult.await !== 'undefined') {
          funcResult.await.then(function (response) {
            return runApply(response, {});
          }).then(function () {
            return continueFunc(state);
          });
        } else {
          runApply(funcResult, {});
          continueFunc();
        }
      });
    } else {
      continueFunc();
    }
  },


  // Creates a promise that waits, process and response to a dispatch future
  componentDispatcherPromise: function componentDispatcherPromise(dispatchID, resolveFunc) {
    var _this4 = this;

    return new Promise(function (resolve, cancel) {
      console.log("Promise");
      console.log("Type: " + dispatchID);

      _this4.props.emitter.removeAllListeners(dispatchID);
      _this4.props.emitter.on(dispatchID, function (data) {
        return resolve(resolveFunc(data));
      });
      _this4.props.dispatcher.dispatch({ type: dispatchID });
    });
  },


  // When a normal button is pressed
  buttonAction: function buttonAction(actions, itemId) {
    var _this5 = this;

    var pureAction = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : null;

    if (itemId !== null) {
      var item = this.state.actions[this.state.active].body.filter(function (i) {
        return i.id === itemId;
      })[0];
      if (typeof item.button.action !== 'undefined') {
        if (typeof item.button.action === 'string') {
          this.setActivePopup(null);
        } else if (item.button.action.popup === true) {
          this.setState({ activePopup: item.button.action.id });
        } else {
          var executedPureState = {};
          this.runScreenInitFunctions(this.state.actions[item.button.action.id], executedPureState, function (passedState) {
            var originalPureState = typeof _this5.state.pureState === 'undefined' ? {} : _this5.state.pureState;
            var newPureState = typeof item.button.action.setState === 'undefined' ? {} : item.button.action.setState;
            var combinedPureState = _this5.consumeThePureState(_extends({}, originalPureState, newPureState), actions, item.button.action.id);
            _this5.setState(_extends({ active: item.button.action.id }, combinedPureState, executedPureState, passedState));
          });
        }
      }
    }

    // TODO: Change from :: to object
    else if (pureAction === 'close::popup') {
        this.setActivePopup(null);
      } else {
        this.setActiveScreen(pureAction);
      }
  },
  consumeThePureState: function consumeThePureState(pureState, actions, id) {
    console.warn("~> Attempt to consume the pure state and set a new default state");
    var addonState = {};
    this.state.actions[id].body.map(function (bodyItem) {
      var itemId = bodyItem.id;
      var mainKey = Object.keys(bodyItem).filter(function (key) {
        return key !== "id";
      })[0];
      Object.keys(bodyItem[mainKey]).map(function (innerItem) {
        if (typeof bodyItem[mainKey][innerItem].state !== 'undefined') {
          if (typeof pureState[bodyItem[mainKey][innerItem].state] !== 'undefined') {
            Object.assign(addonState, _defineProperty({}, itemId + '-' + bodyItem[mainKey][innerItem].state, pureState[bodyItem[mainKey][innerItem].state]));
          }
        }
      });
    });
    Object.assign(addonState, { pureState: _extends({}, pureState) });
    return addonState;
  },


  // If a button is click a few check will happen to determine what the follow-up action should be
  // Generally this function will be used to change colors of the buttons
  buttonTouchTap: function buttonTouchTap(actions, itemId) {
    var _this6 = this;

    // If the generic button click is selectable
    if (this.state[itemId + '-selectable']) {
      // Filter the items in the current action group the get the json body of the itemId active
      var item = this.state.actions[this.state.active].body.filter(function (i) {
        return i.id === itemId;
      })[0];
      // This object will be applied to the state
      var stateSet = {};
      // Invert the current click button state
      stateSet[itemId + '-selected'] = !this.state[itemId + '-selected'];
      var linkGroupFunction = function linkGroupFunction() {
        var groupedButtons = _this6.state.actions[_this6.state.active].body.filter(function (i) {
          return i.id !== itemId && typeof i.button !== 'undefined' && i.button.group == item.button.group;
        });
        groupedButtons.map(function (i) {
          if (_this6.state[i.id + '-selectable']) {
            _this6.assignDynamicState(stateSet, i.id + '-selected', false);
          }
        });
      };

      // If the button should invert to another button in the group on press
      if (typeof item.button.invertGroup !== 'undefined') {
        if (item.button.invertGroup === true) {
          var groupedButtons = this.state.actions[this.state.active].body.filter(function (i) {
            return i.id !== itemId && typeof i.button !== 'undefined' && i.button.group == item.button.group && i.button.selectable;
          });
          // Removed selected from all the buttons to reset the selected state
          groupedButtons.map(function (i) {
            _this6.assignDynamicState(stateSet, i.id + '-selected', false);
          });
          // If the active button is the last in the active group restart the selection
          if (groupedButtons.filter(function (i) {
            return i.button.groupOrder > item.button.groupOrder;
          }).length === 0) {
            var selected = groupedButtons.filter(function (i) {
              return i.button.groupOrder === 0;
            })[0];
            if (typeof selected !== 'undefined' && this.state[itemId + '-selected']) {
              this.assignDynamicState(stateSet, selected.id + '-selected', true);
            }
          }
          // If there is an increment number on the button group select the next buttin
          else {
              var _selected = groupedButtons.filter(function (i) {
                return i.button.groupOrder === item.button.groupOrder + 1;
              })[0];
              if (typeof _selected !== 'undefined' && this.state[itemId + '-selected']) {
                this.assignDynamicState(stateSet, _selected.id + '-selected', true);
              }
            }
        }
        // If the invert group boolean is specified but not true run a linked group test
        else linkGroupFunction();
      }
      /* If the button is linked to a group and not in a invert state run the group function */
      else if (typeof item.button.group !== 'undefined') linkGroupFunction();

      // Set the new button states
      this.setState(stateSet);
    }
  },


  // Hack to set state with text instead of a hardcoded name
  // Quality of life improvement for dynamic state
  setDynamicState: function setDynamicState(id, value) {
    var temp = {};
    temp['' + id] = value;
    this.setState(temp);
  },


  // Hack to assign a id and value to a existing json object in a form of id === string
  // Quality of life improvement for dynamic state
  assignDynamicState: function assignDynamicState(original, id, value) {
    var temp = {};
    temp['' + id] = value;
    Object.assign(original, temp);
  }
});

module.exports = screenProcessor;