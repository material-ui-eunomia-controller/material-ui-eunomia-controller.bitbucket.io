// Old js for compatibility
function addEvent (name, height) {
  console.log(name + "-chip")
  document.getElementById(name + "-chip").addEventListener("click",function(e){
    var open = document.getElementById(name + "-chip").getAttribute("open")
    if (open === null || open === "false") {
      document.getElementById(name + "-chip").setAttribute('open','true')
      document.getElementById(name).style.maxHeight = height + "px"
      document.getElementById(name).style.paddingTop = "20px"
      document.getElementById(name).style.paddingBottom = "20px"
    }
    else {
      document.getElementById(name + "-chip").setAttribute('open','false')
      document.getElementById(name).style.maxHeight = "0px"
      document.getElementById(name).style.paddingTop = "0px"
      document.getElementById(name).style.paddingBottom = "0px"
    }
  },false);
}

addEvent("collapsed-a", 500)
addEvent("collapsed-b", 250)
addEvent("collapsed-c", 500)
addEvent("collapsed-cc", 500)
addEvent("collapsed-d", 900)
addEvent("collapsed-e", 600)
addEvent("collapsed-f", 1500)
addEvent("collapsed-g", 2200)
addEvent("collapsed-h", 800)

addEvent("collapsed-component-a", 2000)
addEvent("collapsed-component-b", 2000)
addEvent("collapsed-component-c", 2000)
addEvent("collapsed-component-d", 2000)
addEvent("collapsed-component-e", 2000)