import React from 'react'
import Paper from 'material-ui/Paper'
import MenuItem from 'material-ui/MenuItem'
import SelectField from 'material-ui/SelectField'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton'
import CircularProgress from 'material-ui/CircularProgress'
import AutoComplete from 'material-ui/AutoComplete'
import Dialog from 'material-ui/Dialog'
import FlatButton from 'material-ui/FlatButton'
import PropTypes from 'prop-types'
const createReactClass = require('create-react-class')

// This config below is for easy access to add more object below
// Most of this script runs generically thus the config below will fit in 99.9% of the times
// TODO: Make a global nonAttribute list instead per instance

const screenConfig = {
  // This is the state applied to the type of object on startup.
  // The state object usually contains a random uui for example 0000-0000-0000-0000-selectable
  // Thus the object will always contain a state for that text that will not conflict by is created in a dynamic way
  initialState: {
    button: ["selectable", "selected", "error"],
    textfield: ["value", "activeError"],
    text: [],
    dropdown: ["value", "selected", {
      id: "dropdown-source", type: "array"
    }],
    autocomplete: [{
      id: "data-source", type: "array"
    }]
  },
  // Restriction contains all the pure information for the element
  // This means: What can the object process (attribute based or not), what happens when a certain action is activated/clicked and/or how the element is generate. (The component/element restriction)
  restrictions: {
    text: {
      // These are attributes that will be excluded when doing a 'all json fields == attributes: id=value'.
      // If for example you leave out body the generic code will render a element like this: <li body={}></li>
      // Almost all components that do not take a body attribute will then complain
      nonAttributes: ["text", "style", "action", "body", "save", "setState"],
      // This is a reference to the component that needs to be generate. i == attributes, ii == body
      element (i, ii) { return <li {...i}>{ii}</li> }
    },

    button: {
      nonAttributes: ["selected", "selectable", "type", "group", "invertGroup", "groupOrder", "style", "action", "body", "dispatch", "dataSource", "group", "setState"],
      element (i) { return <FlatButton {...i} /> },
      // Actions contains all the actions that needs to happen on the id. id == action, value == action function
      actions: {
        onTouchTap: "buttonTouchTap",
        onClick: "buttonAction"
      }
    },

    textfield: {
      nonAttributes: ["label", "group", "groupOrder", "errorText", "style", "action", "body", "dispatch", "dataSource", "group", "setState"],
      element (i) { return <TextField {...i} /> },
      actions: {
        onChange: "textFieldChange",
        onBlur: "textFieldBlur"
      }
    },

    dropdown: {
      nonAttributes: ["style", "action", "body", "dataSource", "dispatch", "options", "group", "setState"],
      element (i, ii) { return <SelectField {...i}>{ii}</SelectField> },
      actions: {
        onChange: "dropdownChange"
      }
    },

    autocomplete: {
      nonAttributes: ["style", "action", "body", "dataSource", "dispatch", "group", "setState"],
      element (i) { return <AutoComplete {...i} />},
      actions: {}
    }
  }
}

// Generate a uuid - This will be used all over
const guid = () => {
  const s4 = () => Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1)
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

// This prevents a infinite loop. Reason: Dynamic Content, Dynamic State, Dynamic State Set, Generic Functions can equal a setState loop
// This needs to be outside of the state and props sections and be unique per import
var doneState = false

const screenProcessor = createReactClass({
    getInitialState() {
      return {
        // The component should only pre process state on props change once per component mounting
        initialProcessingStepComplete: false,
        // All the available actions the the current screen
        actions: {},
        // Is the rendered screen a dialog popup
        dialog: false,
        // The id of the currently rendered screen
        active: '',
        // The id of the currently rendered popup screen (A popup screen is a screen over any other active screen)
        activePopup: ''
      }
    },

    propTypes: {
      emitter: PropTypes.object.isRequired,
      dispatcher: PropTypes.object.isRequired,

      // The json data that needs to be processed into a material design html format
      data: PropTypes.object.isRequired,

      // The object of functions used for callback processing
      processors: PropTypes.object.isRequired
    },

    // Primary render function - This function renders both the active screen and the active popup screen
    render() {
      console.log("-- Latest Screen Props and State --")
      console.log(this.props)
      console.log(this.state)
      return (
        <span>
        {this.generateActiveScreen(this.state.active)}
          {(typeof this.state.activePopup !== null ? this.generateActiveScreen(this.state.activePopup, true) : "")}
      </span>)
    },

    // This function catches any updates to the component. As soon as the component gets valid data a pre processor is kicked of which then locks the initial render (a.k.a this function from resetting the state again)
    componentWillUpdate(data, state) {
      this.processors = this.props.processors
      if (data !== null && !this.state.initialProcessingStepComplete) {
        console.warn("~> Processing a new state")
        this.getScreenReturned(data.data, state)
        state.initialProcessingStepComplete = true;
      }
    },

    /** This is step one to the entire processing of the dynamic components -
     * 1) Set a new dynamic state without a re-render
     * ** This is a core function */
    // Generate the state variables required for the active returned api screen
    getScreenReturned(data, preState) {
      let failure = false
      if (typeof data.init !== 'string') {
        failure = true
        console.error("No top level `init` (String) field found in your json object - The init field is used to initialise a action screen by this id")
      }

      if (typeof data.dialog !== 'boolean') {
        failure = true
        console.error("No top level `dialog` (Boolean) field found in your json object - The init field is used to mark the entire process as a dialog popup or not")
      }

      if (typeof data.actions !== 'object') {
        failure = true
        console.error("No top level `actions` (Object) field found in your json object")
      }
      else {
        Object.keys(data.actions).map(action => {
          if(typeof data.actions[action].title === 'undefined') {
            console.error(`Actions item '${action}' requires a 'title' (String, Dispatch or Pure State) json field`)
            failure = true
          }
        })
      }

      if (typeof data.init === 'string' && typeof data.actions === 'object') {
        if (typeof data.actions[data.init] === 'undefined') {
          console.error(`Init can't find a action in 'actions' with the name '${data.init}'`)
          failure = true
        }
      }


      if (failure)
        return {}

      // The code below generates and populates the state object
      // This object is used to set dynamic state from the screenConfig object
      var state = {}
      Object.keys((typeof data.actions === 'undefined' ? [] : data.actions)).map(key => {
        (typeof data.actions[key].body === 'undefined'? [] : data.actions[key].body).map(item => {
          // Unique id for the session
          const id = guid()
          // Temp object used to store temp state that will later on be applied to the actual state
          let temp = {}
          let listOfStates = (screenConfig.initialState[Object.keys(item).filter(key => key !== "id")[0].toLowerCase()])
          listOfStates = (typeof listOfStates === 'undefined' ? [] : listOfStates)
          listOfStates.map(initState => {
            // The object in the config can either be defined as a json object or a pure string
            // Default return type for a object is always a string. To set a type for a object use the json object
            if (typeof initState === 'object') {
              if (initState.type.toLowerCase() === 'array') return temp[`${id}-${initState.id}`] = []
              else if (initState.type.toLowerCase() === 'string') return temp[`${id}-${initState.id}`] = ''
              else if (initState.type.toLowerCase() === 'boolean') return temp[`${id}-${initState.id}`] = false
              else if (initState.type.toLowerCase() === 'number') return temp[`${id}-${initState.id}`] = 0
              else {
                // Replace activeError with the error name. Makes dynamic content easier on error text
                return temp[`${id}-${(initState === 'activeError' ? 'error' : initState)}`] = (typeof item[Object.keys(item).filter(key => key !== "id")[0].toLowerCase()][initState] === 'undefined' ? '' : item[Object.keys(item).filter(key => key !== "id")[0].toLowerCase()][initState])
              }
            }
            else {
              // Replace activeError with the error name. Makes dynamic content easier on error text
              return temp[`${id}-${(initState === 'activeError' ? 'error' : initState)}`] = (typeof item[Object.keys(item).filter(key => key !== "id")[0].toLowerCase()][initState] === 'undefined' ? '' : item[Object.keys(item).filter(key => key !== "id")[0].toLowerCase()][initState])
            }
          })
          // Assign a randomly generate uuid to every single object. Makes future state control easier
          Object.assign(item, { "id": id })
          // Assign the state variables created and set within temp to the state temp object which will later on be applied to the actual state
          Object.assign(state, temp)
        })

        // Apply a id and state to the title if required
        var title = (typeof data.actions[key].title === 'undefined'? [] : data.actions[key].title)
        if (typeof title === 'object') {
          const id = guid()
          let temp = {}
          temp[`${id}-${title.state}`] = (typeof title.default !== 'undefined' ? title.default : "--")
          Object.assign(state, temp)
          Object.assign(data.actions[key].title, {"id": id})
        }
      })



      var removeListeners = []
      var addListeners = []
      var fullListeners = []
      // This mapping will populate the listener arrays.
      // removeListeners will contain all the objects that needs to be 'emitter.removeAllListeners'
      // addListeners will contain all the objects that needs to be 'emitter.on'
      Object.keys(data.actions).map(key => {
        if (typeof data.actions[key].title["id"] !== 'undefined') {
          // Listener on the title object
          addListeners.push([data.actions[key].title["id"], data.actions[key].title])
        }

        data.actions[key].body.map(body => {
          const mainKey = Object.keys(body).filter(key => key !== "id")[0]
          if (typeof mainKey !== 'undefined') {
            Object.keys(body[mainKey]).map(item => {
              // If the object in your specified json object contains a .emitter and a .processor then it is awaiting data
              if (typeof body[mainKey][item].emitter !== 'undefined' && typeof body[mainKey][item].processor !== 'undefined'){
                removeListeners.push(body[mainKey][item].emitter)
                addListeners.push([body["id"], body[mainKey][item]])
              }
              if (typeof body[mainKey][item].state !== 'undefined'){
                fullListeners.push([body["id"], body[mainKey][item]])
              }
            })
          }
        })
      })

      // Creates a unique list and then removes the listeners. [a,a,b,c,c,c] == [a,b,c]
      removeListeners.filter((value, index, self) => self.indexOf(value) === index ).map(item => this.props.emitter.removeAllListeners(item));

      // Group all the listeners that has the same emitter
      // Reason why we group them is that all the objects that requires the same emitter can be kicked of at the same time with a single emitter. 1 emitter, multiple functions
      var groupedItems = {}
      addListeners.map(item => {
        if (typeof groupedItems[item[1].emitter] === 'undefined')
          groupedItems[item[1].emitter] = [[item[0], item[1].processor, item[1].emitter, item[1].state]]
        else
          groupedItems[item[1].emitter].push([item[0], item[1].processor, item[1].emitter, item[1].state])
      })


      // Apply a single emitter to a emitter id but with multiple functions for all the grouped listeners
      Object.keys(groupedItems).map(key => {
        // Display a emitter connection
        groupedItems[key].map(to => console.warn(`~> Applying a emitter to "${to[0]}"`))

        // Connect to emitter
        this.props.emitter.on(key, (emitterA, emitterB, emitterC) => {
          groupedItems[key].map(to => {
            (() => {
              var thisScope = this
              var addons = {setState: (data) => {
                this.setState({[`${to[0]}-${to[3]}`]: data})
              }}
              var merge = {...thisScope, ...addons}
              this.processors[to[1]].apply(merge, [to[0], emitterA, emitterB, emitterC, to[3]]);
            })()
          })
        })
      })

      // Generate all the state defined objects for the emitter and processor
      fullListeners.map(item => {
        if (typeof item[1].state !== 'undefined') {
          this.assignDynamicState(state, `${item[0]}-${item[1].state}`, (typeof item[1].default === 'undefined' ? "" : item[1].default))
        }
      })

      this.runScreenInitFunctions(data.actions[data.init], state, () => {
        // Assign all the objects to a preState object
        Object.assign(preState, {
          active: data.init,
          actions: data.actions,
          dialog: data.dialog,
          ...state
        })
      })
    },

    /** This is step two to the entire processing of the dynamic components -
     * 2) On any action or parent action when a screen is changed a new actionId is passed to this function which then generates a new screen based on the id
     * ** This is a core function */
      // Generate a new screen with all the relevant data - This happens when the state changes
      generateActiveScreen (actionId, forceDialog = false) {
      // If the actionId is not valid then a active screen can not be generated. actionId == the screen within the json object
      if (typeof actionId === 'undefined' || actionId == null || actionId == '' || actionId == 'undefined') return null
      else {
        // First we check if the screen id you are requesting does exist if not a empty will be generated and a console error will be produced
        const screen = this.state.actions[actionId]
        if (typeof screen === 'undefined'){
          console.error(`~> A screen with the id '${actionId}' not found`)
          return null
        }

        // Generate all the primary buttons that is going to be used within this screen
        // This is all the buttons in either the top of bottom of the window
        // They get generated separately because generally this affects the actual window and not data
        const buttons = (typeof screen.buttons === 'undefined' ? [] : screen.buttons).map((button) => {
          const generatedButton = <RaisedButton secondary={(typeof button.secondary === 'undefined' ? false : button.secondary)} key={guid()} label={button.label} onClick={() => {this.buttonAction(null, button.action)}} />
          if (this.state.dialog || forceDialog) return generatedButton
          else return (<div key={button.label} className='search_result_card_button'>{generatedButton}</div>)
        })

        // This function generates, assigns, structures and applies all the correct data to the elements.
        /** If any special processing is required it will happen here
         * for example a button gets a special color when a type is defined
         * */
        const spawnElement = (id, item, original) => {
          // Currently all objects that can be used needs to be defined in the restrictions object
          // TODO: Remove this and make it more generic - Easier for future use
          if (typeof screenConfig.restrictions[id] === 'undefined') {
            console.error(`~> '${id}' not found in the restrictions object`)
            return <div></div>
          }

          // Id, name and key is preset to the actions object
          // All extra actions on a element is contained in the actions json
          let actions = {id: item.id, name: item.id, key: item.id}

          // Link all element type action to functions
          if (typeof screenConfig.restrictions[id].actions !== 'undefined') {
            Object.keys(screenConfig.restrictions[id].actions).map(action => {
              this.assignDynamicState(actions, action, (...values) => this[screenConfig.restrictions[id].actions[action]](actions, item.id, ...values))
            })
          }

          // Apply all the extra objects as attributes
          let attributes = {}
          Object.assign(attributes, item[id])
          Object.keys(item[id]).map(i => (screenConfig.restrictions[id].nonAttributes.indexOf(i) !== -1 ? delete actions[i] : null))
          Object.keys(item[id]).map(i => (screenConfig.restrictions[id].nonAttributes.indexOf(i) !== -1 ? delete attributes[i] : null))

          // Add a default style object
          let tempStyle = (typeof item[id].style !== 'undefined' ? item[id].style : {})
          Object.assign(actions, {style: {...tempStyle, marginBottom: "14px"}})
          // Add a default margin - Improves the look and feel

          if (id === 'button')  {
            // Assign a state to a label if required.
            // Needs to be hardcoded because of the 'not_int' for word testing
            var label = item[id].label
            if (typeof item[id].label === 'object') {
              if (typeof item[id].label.state !== 'undefined') {
                label = this.state[`${item.id}-${item[id].label.state}`]
              }
            }

            // Applies a color and/or the not_int flag
            Object.assign(actions, {className: `button_${(typeof item[id].type === 'undefined' ? "" : item[id].type)} ${(this.state[`${item.id}-selected`] === false || this.state[`${item.id}-selected`] === '' ? "" : "selected")} ${(/\s+/ig.test(label) ? "not_int" : "")}`})

            // Make sure the component does not break
            if (typeof item[id].label === 'undefined') {
              this.assignDynamicState(actions, "label", " ")
            }
          }

          // If the object is a textfield some special attention is required
          if (id === 'textfield') {
            Object.assign(attributes, { value: this.state[`${item.id}-value`], errorText: this.state[`${item.id}-error`]})
          }

          // If the object is a dropdown some special attention is required
          if (id === 'dropdown') {
            if (typeof item[id].options === 'object' && typeof item[id].options.emitter !== 'undefined') {
              item[id].body = this.state[`${item.id}-dropdown-source`].map(x => <MenuItem key={guid()} value={x} label={x} primaryText={x} />)
              Object.assign(actions, {
                onChange: (event, index, value) => this.dropDownChange(item.id, event, index, value),
                value: this.state[`${item.id}-selected`]
              })
            }
            else {
              item[id].body = item[id].options.map(i => <MenuItem key={guid()} value={i.value} label={i.body} primaryText={i.body} />)
              Object.assign(actions, {
                onChange: (event, index, value) => this.dropDownChange(item.id, event, index, value),
                value: this.state[`${item.id}-selected`]
              })
            }
          }

          // On all dataSources defined we need to set a object state
          // TODO: Make more generic
          if (typeof item[id].dataSource !== 'undefined') {
            Object.assign(attributes, {
              dataSource: this.state[`${item.id}-data-source`]
            })
          }

          // Make sure we do not have any undefined objects that needs to be destructured
          original = (typeof original === 'undefined' ? {} : original)
          actions = (typeof actions === 'undefined' ? {} : actions)
          attributes = (typeof attributes === 'undefined' ? {} : attributes)

          // Assign the user stated state values
          // assignedBody contains the original body and will be modified if a state is set
          var assignedBody = item[id].body
          Object.keys(item[id]).map(key => {
            if (typeof item[id][key].state !== 'undefined') {
              if (key === "body") {
                assignedBody = this.state[`${item.id}-${item[id][key].state}`]
              }
              else {
                this.assignDynamicState(attributes, `${key}`, this.state[`${item.id}-${item[id][key].state}`])
              }
            }
          })

          // Remove all attributes from the actions object that contains private fields. For example emitter and processor objects
          Object.keys(actions).map(key => {
            if (typeof actions[key] !== "undefined") {
              if (typeof actions[key].emitter !== 'undefined' || typeof actions[key].processor !== 'undefined' || typeof actions[key].state !== 'undefined') {
                delete actions[key]
              }
            }
          })

          // Remove all attributes from the attributes object that contains private fields. For example emitter and processor objects
          Object.keys(attributes).map(key => {
            if (typeof attributes[key] !== "undefined") {
              if (typeof attributes[key].emitter !== 'undefined' || typeof attributes[key].processor !== 'undefined' || typeof attributes[key].state !== 'undefined') {
                delete attributes[key]
              }
            }
          })

          // Generate the element
          return screenConfig.restrictions[id].element({...actions, ...original, ...attributes}, (typeof assignedBody !== 'undefined' ? assignedBody : ''))
        }

        /**
         A done state is checked so that we don't run into a infinite loop.
         Render =>
         ^ Render dispatch events from json =>
         |   On render set a dynamic state for the new components =>
         |     If re-render the Render function is called again >-|
         |                                                        |
         -----<---------<---------------<------------<-----------
         Thus we are setting a done state on the initial render
         */

        // This prevents a infinite loop. Reason: Dynamic Content, Dynamic State, Dynamic State Set, Generic Functions can equal a setState loop
        // A dispatch event will run return setState and then the component will try to reRender and redispatch
        if (!doneState) {
          doneState = true
          if (typeof this.state.actions[actionId].dispatch !== 'undefined') {
            this.state.actions[actionId].dispatch.map(dis => {
              if (typeof dis.type !== 'undefined') {
                this.props.dispatcher.dispatch({type: dis.type})
              }
            })
          }
        }
        var dynamicTitle = (typeof screen.title === 'string' ? screen.title : this.state[`${screen.title.id}-${screen.title.state}`])

        let screenObjects = (
          <span>
            <div className='title' style={{marginBottom: "15px", padding: "5px 3px 10px 3px", borderBottom: "1px solid #EBEBEB", fontSize: "20px", width: "100%"}}>
              {dynamicTitle}
            </div>
            <div className='button_bar_dialer' style={{width: "100%", float: "left"}}>
              <div className='buttons' style={{position: "relative", top: "0px"}}>
                <div className='middled' style={{width: "100%", float: "left", clear: "both", paddingBottom: "15px", borderBottom: "1px solid #EBEBEB", marginBottom: "15px"}}>
                  {buttons}
                </div>
              </div>
            </div>
            <ul style={{float: "left", clear: "both"}} className='script'>{(typeof screen.body === 'undefined' ? [] : screen.body).map((item) => spawnElement(Object.keys(item).filter(key => key !== "id")[0].toLowerCase(), item))}</ul>
          </span>
        )

        if (typeof screen.config !== 'undefined') {
          if (typeof screen.config.buttons !== 'undefined') {
            if (typeof screen.config.buttons.position !== 'undefined') {
              if (screen.config.buttons.position === 'bottom') {
                screenObjects = (
                  <span>
                    <div className='title' style={{marginBottom: "15px", padding: "5px 3px 10px 3px", borderBottom: "1px solid #EBEBEB", fontSize: "20px", width: "100%"}}>
                      {dynamicTitle}
                    </div>
                    <ul style={{float: "left", clear: "both"}} className='script'>{(typeof screen.body === 'undefined' ? [] : screen.body).map((item) => spawnElement(Object.keys(item).filter(key => key !== "id")[0].toLowerCase(), item))}</ul>
                    <div className='button_bar_dialer' style={{width: "100%", float: "left"}}>
                      <div className='buttons' style={{position: "relative", top: "0px"}}>
                        <div className='middled' style={{width: "100%", float: "left", clear: "both", paddingTop: "15px", borderTop: "1px solid #EBEBEB", marginTop: "15px", paddingBottom: "8px"}}>
                          {buttons}
                        </div>
                      </div>
                    </div>
                  </span>
                )
              }
              else if (screen.config.buttons.position === 'left') {
                screenObjects = (
                  <span>
                    <div style={{borderBottom: "1px solid #EBEBEB", padding: "5px 3px 10px 3px", marginBottom: "15px", float: "left", width: "100%"}}>
                      <div className='button_bar_dialer' style={{width: "50%", float: "left"}}>
                        <div className='buttons' style={{position: "relative", top: "0px"}}>
                          <div className='middled' style={{width: "100%", float: "left", clear: "both", paddingBottom: "5px"}}>
                            {buttons}
                          </div>
                        </div>
                      </div>
                      <div className='title' style={{fontSize: "20px", width: "50%", float: "left"}}>
                        {dynamicTitle}
                      </div>
                    </div>
                    <ul style={{float: "left", clear: "both"}} className='script'>{(typeof screen.body === 'undefined' ? [] : screen.body).map((item) => spawnElement(Object.keys(item).filter(key => key !== "id")[0].toLowerCase(), item))}</ul>
                  </span>
                )
              }
              else if (screen.config.buttons.position === 'right') {
                screenObjects = (
                  <span>
                    <div style={{borderBottom: "1px solid #EBEBEB", padding: "5px 3px 10px 3px", marginBottom: "15px", float: "left", width: "100%"}}>
                      <div className='title' style={{fontSize: "20px", width: "50%", float: "left"}}>
                        {dynamicTitle}
                      </div>
                      <div className='button_bar_dialer' style={{paddingLeft: "5%", width: "45%", float: "left"}}>
                        <div className='buttons' style={{position: "relative", top: "0px"}}>
                          <div className='middled' style={{width: "100%", float: "left", clear: "both", paddingBottom: "5px"}}>
                            {buttons}
                          </div>
                        </div>
                      </div>
                    </div>
                    <ul style={{float: "left", clear: "both"}} className='script'>{(typeof screen.body === 'undefined' ? [] : screen.body).map((item) => spawnElement(Object.keys(item).filter(key => key !== "id")[0].toLowerCase(), item))}</ul>
                  </span>
                )
              }
            }
          }
        }

        // Either a dialog or a normal div is generated. All depends on the dialog flag
        if (this.state.dialog || forceDialog){
          return (
            <Dialog bodyClassName='decline_reason_class' contentClassName='decline_reason_class' title={dynamicTitle} actions={buttons} modal={false} open={true}>
              {(typeof screen.body === 'undefined' ? [] : screen.body).map((item) => spawnElement(Object.keys(item).filter(key => key !== "id")[0].toLowerCase(), item))}
            </Dialog>
          )
        }
        else {
          return (
            <div className='transcript_details'>
              <Paper className='transcript_paper' style={{float: "left", width: "100%", clear: "both"}}>
                {screenObjects}
              </Paper>
            </div>
          )
        }
      }
    },

    // When a button is pressed set the new state to a active id
    setActiveScreen (actionId) {
      doneState = false
      console.log(`~> Set active screen to "${actionId}"`)
      this.setState({active: actionId})
    },

    // When a button is pressed set the new state to a active id
    setActivePopup (actionId) {
      doneState = false
      console.log(`~> Set active popup screen to "${actionId}"`)
      this.setState({activePopup: actionId})
    },

    dropDownChange (actions, actionId, event, index, value) {
      this.setDynamicState(`${actionId}-selected`, value)
    },

    // TODO: Pure state done
    // On text field change action
    textFieldChange (actions, itemId, obj, value) {
      let oldPureState = this.state.pureState
      let extraState = { pureState: { ...oldPureState } }

      const i = this.state.actions[this.state.active].body.filter(item => item.id === itemId)[0]
      const ii = i[Object.keys(i).filter(key => key !== "id")[0]]
      if (typeof ii.setState !== 'undefined') {
        extraState.pureState[ii.setState] = value
      }

      if (typeof actions.setState !== 'undefined') {
        this.setState({
          [`${itemId}-value`]: value,
          [actions.setState]: value,
          ...extraState
        })
      }
      else {
        this.setState({
          [`${itemId}-value`]: value,
          ...extraState
        })
      }
    },

    // On text field blur
    textFieldBlur (actions, itemId, obj, value) {
      this.setDynamicState(`${itemId}-error`, (typeof this.state[`${itemId}-value`] === 'undefined' || this.state[`${itemId}-value`] == '' || this.state[`${itemId}-value`] == null ? this.state.actions[this.state.active].body.filter(i => i.id === itemId)[0].textfield.errorText : ''))
    },

    runScreenInitFunctions (data, state, continueFunc) {
      if (typeof data.func !== 'undefined') {
        data.func.map(func => {


          var parameters = (typeof func.callStateParameters === 'undefined' ? [] : func.callStateParameters).map(text => (typeof this.state.pureState === 'undefined' ? "" : (typeof this.state.pureState[text] === 'undefined' ? "" : this.state.pureState[text])))

          const runApply = (funcResult, funcResultState) => {
            func.applyTo.map(to => {
              // Apply the value to the title object - seperate cause its a high class object - outside of the body
              if (data.title.state !== 'undefined') {
                if (data.title.state === to.state) {
                  Object.assign(state, {[`${data.title.id}-${data.title.state}`]: funcResult})
                }
              }

              data.body.map(bodyItem => {
                const primary = Object.keys(bodyItem).filter(key => key !== "id")[0]
                Object.keys(bodyItem[primary]).map(internalItem => {
                  if (typeof bodyItem[primary][internalItem].state !== 'undefined') {
                    if (bodyItem[primary][internalItem].state === to.state) {
                      var temp = {...funcResultState}
                      if (funcResult !== null) {
                        temp[`${bodyItem.id}-${bodyItem[primary][internalItem].state}`] = funcResult
                      }
                      Object.assign(state, temp)
                    }
                  }
                })
              })
            })
          }

          const funcResult = this.props.processors[func.call].call(this, ...parameters)

          if (typeof funcResult.await !== 'undefined') {
            funcResult.await
              .then((response) => runApply(response, {}))
              .then(() => continueFunc(state))
          }
          else {
            runApply(funcResult, {})
            continueFunc()
          }
        })
      }
      else {
        continueFunc()
      }
    },

    // Creates a promise that waits, process and response to a dispatch future
    componentDispatcherPromise (dispatchID, resolveFunc) {
      return new Promise ((resolve, cancel) => {
        console.log("Promise")
        console.log("Type: " + dispatchID)

        this.props.emitter.removeAllListeners(dispatchID)
        this.props.emitter.on(dispatchID, (data) => resolve(resolveFunc(data)))
        this.props.dispatcher.dispatch({type: dispatchID})
      })
    },

    // When a normal button is pressed
    buttonAction (actions, itemId, pureAction = null) {
      if (itemId !== null) {
        var item = this.state.actions[this.state.active].body.filter(i => i.id === itemId)[0]
        if (typeof item.button.action !== 'undefined') {
          if (typeof item.button.action === 'string') {
            this.setActivePopup(null)
          }
          else if (item.button.action.popup === true) {
            this.setState({activePopup: item.button.action.id})
          }
          else {
            const executedPureState = {}
            this.runScreenInitFunctions(this.state.actions[item.button.action.id], executedPureState, (passedState) => {
              const originalPureState = (typeof this.state.pureState === 'undefined' ? {}: this.state.pureState)
              const newPureState = (typeof item.button.action.setState === 'undefined' ? {} : item.button.action.setState)
              const combinedPureState = this.consumeThePureState({...originalPureState, ...newPureState}, actions, item.button.action.id)
              this.setState({active: item.button.action.id, ...combinedPureState, ...executedPureState, ...passedState})
            })
          }
        }
      }

      // TODO: Change from :: to object
      else if (pureAction === 'close::popup') {
        this.setActivePopup(null)
      }
      else {
        this.setActiveScreen(pureAction)
      }
    },

    consumeThePureState (pureState, actions, id) {
      console.warn("~> Attempt to consume the pure state and set a new default state")
      let addonState = {}
      this.state.actions[id].body.map(bodyItem => {
        const itemId = bodyItem.id
        const mainKey = Object.keys(bodyItem).filter(key => key !== "id")[0]
        Object.keys(bodyItem[mainKey]).map(innerItem => {
          if (typeof bodyItem[mainKey][innerItem].state !== 'undefined') {
            if (typeof pureState[bodyItem[mainKey][innerItem].state] !== 'undefined') {
              Object.assign(addonState, {[`${itemId}-${bodyItem[mainKey][innerItem].state}`]: pureState[bodyItem[mainKey][innerItem].state]})
            }
          }
        })
      })
      Object.assign(addonState, {pureState: {...pureState}})
      return addonState
    },

    // If a button is click a few check will happen to determine what the follow-up action should be
    // Generally this function will be used to change colors of the buttons
    buttonTouchTap (actions, itemId) {
      // If the generic button click is selectable
      if(this.state[`${itemId}-selectable`]) {
        // Filter the items in the current action group the get the json body of the itemId active
        var item = this.state.actions[this.state.active].body.filter(i => i.id === itemId)[0]
        // This object will be applied to the state
        let stateSet = {}
        // Invert the current click button state
        stateSet[`${itemId}-selected`] = !this.state[`${itemId}-selected`]
        const linkGroupFunction = () => {
          const groupedButtons = this.state.actions[this.state.active].body.filter(i => {
            return (i.id !== itemId && typeof i.button !== 'undefined' && i.button.group == item.button.group)
          })
          groupedButtons.map(i => {
            if (this.state[`${i.id}-selectable`]) {
              this.assignDynamicState(stateSet, `${i.id}-selected`, false)
            }
          })
        }

        // If the button should invert to another button in the group on press
        if (typeof item.button.invertGroup !== 'undefined') {
          if (item.button.invertGroup === true) {
            const groupedButtons = this.state.actions[this.state.active].body.filter(i => (i.id !== itemId && typeof i.button !== 'undefined' && i.button.group == item.button.group && i.button.selectable))
            // Removed selected from all the buttons to reset the selected state
            groupedButtons.map(i => {
              this.assignDynamicState(stateSet, `${i.id}-selected`, false)
            })
            // If the active button is the last in the active group restart the selection
            if (groupedButtons.filter(i => i.button.groupOrder > item.button.groupOrder).length === 0) {
              const selected = groupedButtons.filter(i => i.button.groupOrder === 0)[0]
              if(typeof selected !== 'undefined' && this.state[`${itemId}-selected`]){
                this.assignDynamicState(stateSet, `${selected.id}-selected`, true)
              }
            }
            // If there is an increment number on the button group select the next buttin
            else {
              const selected = groupedButtons.filter(i => i.button.groupOrder === item.button.groupOrder + 1)[0]
              if(typeof selected !== 'undefined' && this.state[`${itemId}-selected`]){
                this.assignDynamicState(stateSet, `${selected.id}-selected`, true)
              }
            }
          }
          // If the invert group boolean is specified but not true run a linked group test
          else linkGroupFunction()
        }
        /* If the button is linked to a group and not in a invert state run the group function */
        else if (typeof item.button.group !== 'undefined') linkGroupFunction()

        // Set the new button states
        this.setState(stateSet)
      }
    },

    // Hack to set state with text instead of a hardcoded name
    // Quality of life improvement for dynamic state
    setDynamicState (id, value) {
      let temp = {}
      temp[`${id}`] = value
      this.setState(temp)
    },

    // Hack to assign a id and value to a existing json object in a form of id === string
    // Quality of life improvement for dynamic state
    assignDynamicState (original, id, value) {
      let temp = {}
      temp[`${id}`] = value
      Object.assign(original, temp)
    }
  })

module.exports = screenProcessor
